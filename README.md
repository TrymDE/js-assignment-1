
# Javascript assignment 1 - Komputer Store

This project is my solution for the first assignment of the Java fullstack developer course for Experis Academy.

## Usage

This program consists of three major sections, divided into four different UI elements. The three sections consists of the Bank, Work and Laptop section, and each section has several functions.

In the bank the user can:
* Get a loan using the "get a loan" button. The user is only allowed to get a loan if they have no other loans who haven't been repaid, and they can only get a loan twice the size of their current balance.


In work the user can:
* Click a button to gain money.
* Transfer money from the work balance to the bank balance.
* Repay a loan, if the user has one, using the "repay loan" button. This button only shows up if the user already has a loan that they haven't repaid yet.

Finally, for the laptop section the user can:
* Use a drop down menu to select a laptop. This displays some info about the selected laptop.
* Buy the selected laptop. This removes money from the bank balance, if there is enough.

## Author

Trym Dammann Ellingsen