
const getLoanBtn = document.getElementById("loanBtn")
const payLoanBtn = document.getElementById("payLoanBtn");
const transferBtn = document.getElementById("bankTransferBtn")
const workBtn = document.getElementById("workBtn")
const fillerDiv = document.getElementById("fillerDiv")
const showLoan = document.getElementById("showLoan")
const featureList = document.getElementById("features")
const laptopSelect = document.getElementById("laptopSelect")
const laptopTitle = document.getElementById("infoTitle")
const laptopDesc = document.getElementById("infoDesc")
const laptopPrice = document.getElementById("infoPrice")
const buyBtn = document.getElementById("buyBtn")
const laptopImg = document.getElementById("laptopImg")

imgBaseURL = "https://noroff-komputer-store-api.herokuapp.com/"

// Bank class. Contains all logic of the bank element from index.html
class Bank {

    outstandingLoan = 0
    balance = 200
    // Useful for updating the text in the UI
    balanceTextElem
    loanTextElem

    constructor () {
        this.balanceTextElem = document.getElementById("balanceNum")
        this.loanTextElem = document.getElementById("loanNum")
    }

    addLoan(amount) {

        /* Check whether user is able to get a new loan, send an error message if they can't.
        Check has been split into two, so the user can get a more detailed error message */
        if(this.outstandingLoan > 0) {
            alert("Can't get new loan right now. Pay back your outstanding loan")

            /* returns are added just so we don't accidentally give an invalid loan, the
            returned value is not actually used */
            return false
        }

        if(amount > this.balance*2) {
            alert("Can't get loan higher than two times your current balance")
            return false
        }

        // Finally, apply the loan.
        this._addLoan(amount)

    }

    // The update"Var" methods simply update the UI to show the current value, in all cases.
    updateBalance() {
        this.balanceTextElem.textContent = `${this.balance} kr`
    }

    updateLoan() {
        this.loanTextElem.textContent = `${this.outstandingLoan} kr`
    }

    addBalance(amount) {
        this.balance += amount
        this.updateBalance()
    }

    toggleLoan() {
        // Shows loan text and repay loan button. Should be called every time we add a new loan /
        // every time we repay a previous loan.
        showLoan.hidden = !showLoan.hidden
        work.toggleTransfer()
    }

    _addLoan(value) {
        // We can only add a loan if the previous loan value is 0
        if (!value) {
            return
        }
        this.outstandingLoan = value
        this.addBalance(value)
        this.updateLoan()
        this.toggleLoan()
    }

    payLoan(value) {
        // Repays the loan
        this.outstandingLoan -= value
        // If loan was repaid in full...
        if (this.outstandingLoan <= 0) {
            // ... remove the loan text and button
            this.toggleLoan()
            // ... and add the remaining money to the bank balance
            this.addBalance(-this.outstandingLoan)
        } else {
            // If not, show the new loan value instead.
            this.updateLoan()
        }
        // We don't need to set a new loan value if it is negative, as it is reset in the addLoan function
        // and we can assume a loan below 0 is equal to 0
    }
}

class Work {

    pay = 0
    payTextElem

    constructor () {
        this.payTextElem = document.getElementById("payNum")
    }

    // Shows or hides the repay loan button. Should be called every time we add a new loan or repay
    // an old loan in full.
    toggleTransfer() {
        payLoanBtn.hidden = !payLoanBtn.hidden
    }

    // Changes the ui to match the Work class
    updatePay() {
        this.payTextElem.textContent = `${this.pay} kr`
    }

    work() {
        this.pay += 100
        this.updatePay()
    }

    // Pay back loan using entirety of the salary. Potential overflow is handled in the bank
    payBack() {
        bank.payLoan(this.pay)
        this.pay = 0
        this.updatePay()
    }

    transferToBank() {
        if(bank.outstandingLoan > 0) {
            // Send 10% of income to loan and the rest to bank
            const toLoan = 0.1*this.pay
            const toBank = this.pay-toLoan
            bank.payLoan(toLoan)
            bank.addBalance(toBank)
        } else {
            // if no loan, add all to bank
            bank.addBalance(this.pay)
        }
        // After transferring, pay will always be 0.
        this.pay = 0
        this.updatePay()
    }
}

class Laptops {
    
    laptopArray = []

    // Get the laptops from the api
    fetchLaptops() {
        fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
        .then(response => response.json())
        // Add the laptops to the laptop array and add them to the select
        .then(laptopData => {
            this.laptopArray = laptopData
            this.addToSelect()
        })
    }

    showLaptop(laptopIndex) {
        
        // Make sure we empty the previous data in ui, if any.
        // This is to make sure we only show the relevant data
        featureList.innerHTML = ""

        // This will be the laptop object we want to show the specs for.
        const toShow = this.laptopArray[laptopIndex]
    
        for (let index = 0; index < toShow.specs.length; index++) {
            const text = toShow.specs[index];
            // Add all the specs to the list.
            featureList.insertAdjacentHTML("beforeend",
                "<li>" + text + "</li>")
        }

        // TODO show the laptop in the buy / info element as well.
        laptopTitle.textContent = toShow.title
        laptopDesc.textContent = toShow.description
        laptopPrice.textContent = `${toShow.price} NOK`
        laptopImg.src = imgBaseURL + toShow.image

    }

    addToSelect() {

        // Show the first laptop as a default, as this is what will be shown as active in the selector
        this.showLaptop(0)

        for (let index = 0; index < this.laptopArray.length; index++) {
            // Add every laptop to the selector
            const laptop = this.laptopArray[index];
            // We use the index as an identifier, so we can just put it straight into the laptop array
            // when we want to get the relevant laptop information.
            laptopSelect.insertAdjacentHTML("beforeend", 
                `<option value="${index}">${laptop.title}</option>`
            )
        }
    }

    // input parameter is the index of the laptop to buy in the laptop array
    buySelected(index) {
        
        const toBuy = this.laptopArray[index]
        // Convert the price to number once, as we need it several places
        const price = Number(toBuy.price)

        if (price > bank.balance) {
            // I know the ` stings should automatically newline, but for me it was a newline and a tab
            // so I just split it up for it to look better in the editor.
            alert(
            `Not enough money to buy ${toBuy.title}.`+
            `\nYour balance: ${bank.balance} kr\nRequired: ${toBuy.price} kr`)
            return false
        }

        bank.balance -= price
        bank.updateBalance()
        // yay!
        alert(`Purchase successful.\nYour purchase: ${toBuy.title}`)
    }
}

const bank = new Bank()
const work = new Work()
const laptops = new Laptops()
bank.updateBalance()
work.updatePay()
laptops.fetchLaptops()

getLoanBtn.addEventListener("click", () => {

    // Takes input from user to try to add to loan
    const input = prompt("Enter loan amount:");

    // Input is a string. Try to make it a number
    const numInput = Number(input)

    // If input didn't turn into number, shame the user
    if (isNaN(numInput)) {
        alert(`${input} is not a number`)
        return false
    }
    
    // Attempt to add the loan
    bank.addLoan(numInput)

})

payLoanBtn.addEventListener("click", () => {
    work.payBack()
})

transferBtn.addEventListener("click", () => {
    work.transferToBank()
})

workBtn.addEventListener("click", () => {
    work.work()
})

laptopSelect.addEventListener("change", function() {
    // The index in the laptop array for the relevant laptop.
    const laptopIndex = this.value

    // Show the selected laptop.
    laptops.showLaptop(laptopIndex)
})

buyBtn.addEventListener("click", () => {
    // Buy the selected laptop.
    laptops.buySelected(laptopSelect.value)
})